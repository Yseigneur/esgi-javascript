import { isClass } from './check.js'

export const render = (domEl, el) => {
    if(isClass(el)){
        const cmpnt = new el;

        if ((cmpnt.render()).length === undefined) {
        	domEl.appendChild(cmpnt.render());
        } else {
        	for (let i = 0; i < (cmpnt.render()).length; i++) {
	        	domEl.appendChild((cmpnt.render())[i]);
	        }
        }
    } else {
        domEl.appendChild(el);
    }
    
}