import { createElement } from '../../utils/react.js';
import { Component } from '../../utils/component.js';

export class Compteur extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count : 0
        }
    }
    
    addOne = () => {
        this.setState({
            count: this.state.count +1
        });
    };

    removeOne = () => {
        this.setState({
            count: this.state.count -1
        });
    };

    render() {
        let data = [];
    	let p = createElement('p', null, 'Mon compteur : ');
        let pCompteur = createElement('p', {id : "state_count"}, this.state.count);
    	let buttonAdd = createElement('button', {onClick: () => this.addOne()}, 'Ajouter');
        let buttonRemove = createElement('button', {onClick: () => this.removeOne()}, 'Retirer');

    	data.push(p);
        data.push(pCompteur);
    	data.push(buttonAdd);
        data.push(buttonRemove);

        return data;
    }
}