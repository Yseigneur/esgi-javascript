import { createElement } from '../../utils/react.js';
import { Component } from '../../utils/component.js';

export class Err404 extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return createElement('p', null, `404 Error`);
    }
}