import { createElement } from '../../utils/react.js';
import { Component } from '../../utils/component.js';

export class Home extends Component {
    constructor(props) {
        super(props)
    }
    
    render() {
        return createElement('h1', null, 'HOME PAGE');
    }
}