String.prototype.ucfirst = function () {
    if (this.length === 0) return "";
    return this.charAt(0).toUpperCase() + this.substring(1);
};

console.log("hello".ucfirst());



String.prototype.capitalize = function () {

    if (this.length === 0) return "";

    return this
        .toLowerCase()
        .split(" ")
        .map(str => {
            return str.ucfirst();
        })
        .join(" ");
}
console.log("hello world".capitalize());


String.prototype.camelCase = function () {
    if (this.length === 0) return "";

    return this.toLowerCase()
        .split(" ")
        .join("");
}

console.log("hello world".camelCase());

function prop_access(object, value) {
    let objTemp = object;
    for (var i = 0; i < value.split(".").length; i++) {
        let s = value.split(".").map(val => {
            if (objTemp[val]) {
                return val;
            } else {
                return null;
            }
        });
        objTemp = objTemp[s[i]];
    }
    return objTemp;
}