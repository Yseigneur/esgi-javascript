function type_check_v1(arg1, arg2) {
    switch (typeof arg1) {
        case "object":
            if (Array.isArray(arg1)) return type === "array";
            if (arg1 === null) return type === "null";
        default:
            return typeof arg1 === arg2.toLowerCase();
    }
}

function type_check_v2(arg1, object) {
    for (key in object) {
        switch (key) {
            case "type":
                if (!type_check_v1(arg1, object[key])) return false;
                break;
            case "value":
                if (JSON.stringify(arg1) !== JSON.stringify(object[key])) return false;
                break;
            case "enum":
                if (
                    object[key].filter(obj => {
                        JSON.stringify(arg1) !== JSON.stringify(obj);
                    })
                )
                    return false;
                break;
        }
    }
    return true;
}

console.log(type_check_v1("aa", "string"));

console.log(
    type_check_v2({ prop1: 2 }, { type: "object", value: { prop1: 2 } })
);

// todo type_check_v3